from internal.bucket.buckets import *


l_record = collected_ad_col.find({})
for record in l_record:
    if 'city' not in record or record['city'].strip() == '':
        for city_name in l_city_name:
            if record['zone_text'].find(city_name) or record['address'].find(city_name):
                record['city'] = city_name
                break

    if 'district' not in record or record['district'].strip() == '':
        for district_name in l_district_name:
            if record['zone_text'].find(district_name) or record['address'].find(district_name):
                record['district'] = district_name
                break

    collected_ad_col.replace_one({'_id': record['_id']}, record)

print("Finish")
