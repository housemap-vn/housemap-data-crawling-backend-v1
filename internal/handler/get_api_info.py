import json
import time
import sys

import tornado.ioloop
import tornado.web
import traceback
from bson import ObjectId

from internal.bucket.buckets import l_city_asset, l_project_name


class GetApiInfoHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header('Content-Type', 'application/json')

        self.set_status(200)

    def get(self):
        d_res = {
            "status": "ok",
            "data": {
                'city_list': l_city_asset,
                'project_list': l_project_name
            }
        }

        self.write(d_res)
        self.finish()
