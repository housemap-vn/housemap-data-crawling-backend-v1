import json
import time
import sys

import tornado.ioloop
import tornado.web
import traceback
from bson import ObjectId

from internal.schema.ad_schema import AdSchema
from internal.bucket.buckets import collected_ad_col, temp_ad_col, posted_ad_col, TYPE_TEMP_AD, TYPE_COLLECTED_AD, TYPE_POSTED_AD, LOGGER


class UpsertAdHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header('Content-Type', 'application/json')

        self.set_status(500)

    def options(self):
        self.post()

    def post(self):
        d_res = {
            "status": "ok",
            "data": {}
        }

        try:
            d_data = tornado.escape.json_decode(self.request.body)
            d_data['created_at'] = int(time.time())
            ad_type = self.get_argument('ad_type', default=0)

            if ad_type == TYPE_COLLECTED_AD:
                current_ad_col = collected_ad_col

            elif ad_type == TYPE_TEMP_AD:
                current_ad_col = temp_ad_col

            elif ad_type == TYPE_POSTED_AD:
                current_ad_col = posted_ad_col
            else:
                raise Exception("Ad_type not correct")

            d_ad_info = AdSchema().load(d_data)
            if d_ad_info['_id'] == '' or d_ad_info['_id'] is None:
                del d_ad_info['_id']

                result = current_ad_col.insert(d_ad_info)
                d_ad_info['_id'] = str(result)
            else:
                d_ad_info['_id'] = ObjectId(d_ad_info['_id'])
                result = current_ad_col.update({'_id': d_ad_info['_id']}, d_ad_info, upsert=True)

                d_ad_info['_id'] = str(d_ad_info['_id'])

            d_res['data'] = d_ad_info
            self.set_status(200)

        except Exception as e:
            d_res['status'] = 'error'
            d_res['data']['error'] = 'Internal server error'
            d_res['data']['message'] = str(e)

            LOGGER.error(str(e))
            traceback.print_exc(file=sys.stdout)

        self.write(d_res)
        self.finish()
