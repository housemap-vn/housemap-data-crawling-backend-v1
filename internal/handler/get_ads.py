import json
import time
import sys
import pymongo

import tornado.ioloop
import tornado.web
import traceback

from internal.bucket.buckets import collected_ad_col, temp_ad_col, posted_ad_col, TYPE_TEMP_AD, TYPE_COLLECTED_AD, TYPE_POSTED_AD, LOGGER
from internal.schema.ad_schema import AdSchema


class GetAdsHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header('Content-Type', 'application/json')

        self.set_status(500)

    def get(self):
        d_res = {
            "status": "ok",
            "data": {}
        }

        l_ad_record = []
        try:
            city = self.get_argument('city', default='').strip()
            district = self.get_argument('district', default='').strip()
            customer_name = self.get_argument('customer_name', default='').strip()
            project = self.get_argument('project', default='').strip()
            ad_type = self.get_argument('ad_type', default='').strip()

            if ad_type == TYPE_COLLECTED_AD:
                current_ad_col = collected_ad_col

            elif ad_type == TYPE_TEMP_AD:
                current_ad_col = temp_ad_col

            elif ad_type == TYPE_POSTED_AD:
                current_ad_col = posted_ad_col
            else:
                raise Exception("Ad_type not correct")

            # Add condition here
            page = int(self.get_argument('page', default=1))
            page_size = int(self.get_argument('page_size', default=10))

            n_skip = (page - 1) * page_size

            d_condition = {
                'city': city,
                'district': district,
                'project': project,
                'customer_name': customer_name
            }

            if city == '':
                del d_condition['city']

            if district == '':
                del d_condition['district']

            if project == '':
                del d_condition['project']

            if customer_name == '':
                del d_condition['customer_name']

            l_raw_result = current_ad_col.find(d_condition).sort([('created_at', pymongo.DESCENDING)]).limit(page_size).skip(n_skip)

            total_ad = current_ad_col.count({})
            for raw_result in l_raw_result:
                try:
                    l_ad_record.append(AdSchema().load(raw_result, partial=True))

                except Exception as cur_e:
                    LOGGER.error(str(cur_e))

            d_res['data']['records'] = l_ad_record
            d_res['data']['total_ad'] = total_ad

            self.set_status(200)

        except Exception as e:
            d_res['status'] = 'error'
            d_res['data']['error'] = 'Internal server error'
            d_res['data']['message'] = str(e)

            LOGGER.error(str(e))
            traceback.print_exc(file=sys.stdout)

        self.write(d_res)
        self.finish()
