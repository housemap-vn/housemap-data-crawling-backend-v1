import json
import time
import sys

import tornado.ioloop
import tornado.web
import traceback
from bson import ObjectId

from internal.schema.ad_schema import AdSchema
from internal.bucket.buckets import collected_ad_col, temp_ad_col, posted_ad_col, TYPE_TEMP_AD, TYPE_COLLECTED_AD, TYPE_POSTED_AD, LOGGER


class UpdateAdLive(tornado.web.RequestHandler):
    def initialize(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header('Content-Type', 'application/json')

        self.set_status(500)

    def options(self):
        self.post()

    def post(self):
        d_res = {
            "status": "ok",
            "data": {}
        }

        try:
            d_data = tornado.escape.json_decode(self.request.body)

            ad_type = TYPE_POSTED_AD

            if ad_type == TYPE_COLLECTED_AD:
                current_ad_col = collected_ad_col

            elif ad_type == TYPE_TEMP_AD:
                current_ad_col = temp_ad_col

            elif ad_type == TYPE_POSTED_AD:
                current_ad_col = posted_ad_col
            else:
                raise Exception("Ad_type not correct")

            for _id in d_data:
                result = current_ad_col.find_one_and_update({'_id': ObjectId(_id)}, {"$set": {"is_live": True}})

            self.set_status(200)

        except Exception as e:
            d_res['status'] = 'error'
            d_res['data']['error'] = 'Internal server error'
            d_res['data']['message'] = str(e)

            LOGGER.error(str(e))
            traceback.print_exc(file=sys.stdout)

        self.write(d_res)
        self.finish()
