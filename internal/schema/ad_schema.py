from marshmallow import Schema, fields, validates, ValidationError, pre_load, EXCLUDE
from internal.bucket.buckets import TYPE_POSTED_AD, TYPE_TEMP_AD, TYPE_COLLECTED_AD
from bson import ObjectId


class AdSchema(Schema):
    _id = fields.Str(missing='', allow_none=True)
    id_raw = fields.Int(missing=0, allow_none=True)

    url = fields.Str(missing='', allow_none=True)
    title = fields.Str(missing='', allow_none=True)
    description = fields.Str(missing='', allow_none=True)

    ad_need = fields.Str(missing='', default='', allow_none=True)
    ad_category = fields.Str(missing='', default='', allow_none=True)
    ad_is_owned = fields.Boolean(missing=False, default=False, allow_none=True)
    article_type = fields.Str(missing='', allow_none=True)

    is_live = fields.Boolean(missing=False, default=False, allow_none=True)

    raw_price = fields.Str(missing='', allow_none=True)
    raw_area = fields.Str(missing='', allow_none=True)

    publish_date = fields.Str(missing='', allow_none=True)

    customer_name = fields.Str(missing='', allow_none=True)
    customer_mobile_number = fields.Str(missing='', allow_none=True)
    customer_address = fields.Str(missing='', allow_none=True)
    email = fields.Str(missing='', allow_none=True)

    zone_text = fields.Str(missing='', allow_none=True)
    city = fields.Str(missing='', allow_none=True)
    district = fields.Str(missing='', allow_none=True)
    address = fields.Str(missing='', allow_none=True)

    raw_bedroom = fields.Str(missing='', allow_none=True)
    raw_toilet = fields.Str(missing='', allow_none=True)

    project = fields.Str(missing='', allow_none=True)
    project_owner = fields.Str(missing='', allow_none=True)
    project_size = fields.Str(missing='', allow_none=True)

    keyword = fields.List(fields.Str(missing=[]), allow_none=True)
    thumbnail = fields.List(fields.Str(missing=[]), allow_none=True)

    price = fields.Int(missing=0, allow_none=True)
    area = fields.Int(missing=0, allow_none=True)
    bedroom = fields.Int(missing=0, allow_none=True)
    toilet = fields.Int(missing=0, allow_none=True)
    furniture = fields.Str(missing='', allow_none=True)

    lat = fields.Float(missing=0.0, allow_none=True)
    lng = fields.Float(missing=0.0, allow_none=True)

    highway = fields.Int(missing=0, allow_none=True)
    lui_truoc = fields.Int(missing=0, allow_none=True)
    lui_sau = fields.Int(missing=0, allow_none=True)
    lui_hong = fields.Int(missing=0, allow_none=True)
    limit_floor = fields.Int(missing=0, allow_none=True)
    direction = fields.Str(missing='', allow_none=True)
    pool = fields.Boolean(missing=False, default=False, allow_none=True)
    garden = fields.Boolean(missing=False, default=False, allow_none=True)
    tang_ham = fields.Boolean(missing=False, default=False, allow_none=True)
    the_chap = fields.Boolean(missing=False, default=False, allow_none=True)

    created_at = fields.Int(missing=0, allow_none=True)

    class Meta:
        unknown = EXCLUDE

    @pre_load
    def convert_field(self, data, **kwargs):
        if '_id' not in data:
            data['_id'] = ''

        if 'id_raw' not in data:
            data['id_raw'] = 0

        if 'keyword' in data:
            if data['keyword'] == '' or data['keyword'] is None:
                data['keyword'] = []

        if 'thumbnail' in data:
            if data['thumbnail'] == '' or data['thumbnail'] is None:
                data['thumbnail'] = []

        if 'ad_need' in data:
            if data['ad_need'] == '' or data['ad_need'] is None:
                data['ad_need'] = ''
        else:
            data['ad_need'] = ''

        if 'ad_category' in data:
            if data['ad_category'] == '' or data['ad_category'] is None:
                data['ad_category'] = ''
        else:
            data['ad_category'] = ''

        if 'ad_is_owned' in data:
            if data['ad_is_owned'] == '' or data['ad_is_owned'] is None:
                data['ad_is_owned'] = False
        else:
            data['ad_is_owned'] = False

        if 'is_live' in data:
            if data['is_live'] == '' or data['is_live'] is None:
                data['is_live'] = False
        else:
            data['is_live'] = False

        data['_id'] = str(data['_id'])
        return data
