import json
import requests

from bs4 import BeautifulSoup
from internal.bucket.config import d_mapping_iab_playstore, d_mapping_iab_appstore, d_mapping_iab_svm, svm_endpoint, language_detection_endpoint, stm_endpoint


class GetIabSegment:
    def __init__(self):
        pass

    def get_iab_segment(self, app_os, bundle_id):
        if app_os == 'ios':
            return self.__get_ios_segment(bundle_id)
        elif app_os == 'android':
            return self.__get_android_segment(bundle_id)
        else:
            return []

    def __get_language(self, txt):
        res = requests.post(language_detection_endpoint, data={"text": txt})
        d_result = json.loads(res.text.strip())

        if d_result['success']:
            return d_result['languages'][0]['code']

        return ''

    def __get_stm_category(self, en_description):
        res = requests.post(stm_endpoint, data={"content": en_description, "language": "en"})
        d_result = json.loads(res.text.strip())
        return d_result['segments']

    def __get_tier_2(self, l_tier_1, description):
        # HACK to get tier_2
        res = requests.post(svm_endpoint, data={"text": json.dumps({"text": description, "tier_1": l_tier_1}), "language": "en"})
        l_result = json.loads(res.text.strip())
        l_tier_2 = list(map(lambda segment: segment['label'], l_result['taxonomy']))
        return l_tier_2

    def __get_android_segment(self, bundle_id):
        def get_playstore_category(soup):
            l_selected = soup.select('a[itemprop="genre"]')
            l_cat = []
            for selected in l_selected:
                href = selected['href']
                l_split = href.split('apps/category/')
                if len(l_split) < 2:
                    continue
                l_cat.append(l_split[1])

            return l_cat

        def get_tier_1(soup, en_description):
            l_playstore_cat = get_playstore_category(soup)
            l_playstore_iab = filter(lambda segment: segment is not None,
                map(lambda cat: None if cat not in d_mapping_iab_playstore else d_mapping_iab_playstore[cat], l_playstore_cat))

            l_stm_iab = []
            if en_description != '':
                l_stm_iab = self.__get_stm_category(en_description)

            l_iab = list(map(lambda segment: segment.split('-')[0], set(sum(l_playstore_iab, l_stm_iab))))
            l_iab_svm = list(filter(lambda svm_label: svm_label is not None,
                map(lambda segment: None if segment not in d_mapping_iab_svm else d_mapping_iab_svm[segment], l_iab)))

            return l_iab_svm

        l_iab_segment = []

        res = requests.get('https://play.google.com/store/apps/details?id={}&hl=en'.format(bundle_id))
        soup = BeautifulSoup(res.text.strip(), 'html.parser')

        selected = soup.find('meta', property='og:description')
        description = '' if selected is None else selected['content'].strip()

        selected = soup.select_one('div[jsname="Igi1ac"]')
        translated_description = '' if selected is None else selected.text.strip()

        en_description = ''
        if self.__get_language(description) == 'en':
            en_description = description
        elif self.__get_language(translated_description) == 'en':
            en_description = translated_description

        l_tier_1 = get_tier_1(soup, en_description)
        if en_description == '' or len(en_description.split(' ')) < 5:
            l_iab_segment = list(map(lambda segment: segment.title(), l_tier_1))
        else:
            l_iab_segment = self.__get_tier_2(l_tier_1, en_description)

        return list(set(l_iab_segment))

    def __get_ios_segment(self, bundle_id):
        def get_tier_1(l_genre_id, en_description):
            l_appstore_iab = filter(lambda segment: segment is not None,
                map(lambda genre_id: None if genre_id not in d_mapping_iab_appstore else d_mapping_iab_appstore[genre_id], l_genre_id))

            l_stm_iab = []
            if en_description != '':
                l_stm_iab = self.__get_stm_category(en_description)

            l_iab = list(map(lambda segment: segment.split('-')[0], set(sum(l_appstore_iab, l_stm_iab))))
            l_iab_svm = list(filter(lambda svm_label: svm_label is not None,
                map(lambda segment: None if segment not in d_mapping_iab_svm else d_mapping_iab_svm[segment], l_iab)))

            return l_iab_svm

        if not bundle_id.isdigit():
            res = requests.get('https://itunes.apple.com/lookup?bundleId={}'.format(bundle_id))
        else:
            res = requests.get('https://itunes.apple.com/lookup?id={}'.format(bundle_id))
        d_app_info = json.loads(res.text.strip())

        if d_app_info['resultCount'] < 1:
            return []

        d_app_info = d_app_info['results'][0]
        l_genre_id = d_app_info['genreIds']
        description = d_app_info['description']

        en_description = ''
        if self.__get_language(description) == 'en' or len(en_description.split(' ')) < 5:
            en_description = description

        l_tier_1 = get_tier_1(l_genre_id, en_description)
        if en_description == '':
            l_iab_segment = list(map(lambda segment: segment.title(), l_tier_1))
        else:
            l_iab_segment = self.__get_tier_2(l_tier_1, en_description)

        return list(set(l_iab_segment))

