import json
import grequests
import requests
import dateparser
import time
import numpy as np

from datetime import datetime

from bs4 import BeautifulSoup
from collections import defaultdict
from heapq import nlargest

from internal.constant.play_store import HL_LANGUAGE_CODES
from internal.bucket.config import language_detection_endpoint


class GetAppLang:
    def __init__(self):
        pass

    def get_app_lang(self, app_os, bundle_id):
        if app_os == 'ios':
            return self.__get_ios_app_lang(bundle_id)
        elif app_os == 'android':
            return self.__get_android_app_lang(bundle_id)
        else:
            return []

    def get_app_lang_by_review(self, app_os, bundle_id):
        if app_os == 'android':
            return self.__get_android_app_lang_by_review(bundle_id)
        else:
            return []

    def __get_android_app_lang_by_review(self, bundle_id):
        def get_timestamp(date_string, lang_code):
            # Korean date string have format: 2019년 6월 11일
            # Thai date string format: 14 กุมภาพันธ์ 2562 (Real day 14-06-2019) need to minus 543 year.
            # Thai date is wrong on year only, day and month are OK
            if lang_code == 'ko':
                try:
                    return time.mktime(datetime.strptime(date_string, "%Y년 %m월 %d일").timetuple())
                except Exception:
                    return None
            else:
                try:

                    parsed_date = dateparser.parse(date_string)
                    day = parsed_date.day
                    month = parsed_date.month
                    year = parsed_date.year if lang_code != 'th' else parsed_date.year - 543

                    dt = datetime(year, month, day)
                    return int(time.mktime(dt.timetuple()))

                except Exception:
                    return None

            return None

        l_review_timestamp = []
        l_pre_review_content = []
        l_review_content = []

        l_req = []
        for lang_code in HL_LANGUAGE_CODES:
            for i in range(1):
                payload = {
                    'pageNum': i,
                    'id': bundle_id,
                    'reviewSortOrder': 0,
                    'hl': lang_code,
                    'reviewType': 1,
                    'xhr': 1
                }

                res = requests.post('https://play.google.com/store/getreviews', data=payload)
                d_result = json.loads(res.text[6:])
                raw_content = d_result[0][2]
                if len(raw_content.strip()) < 10:
                    break

                soup = BeautifulSoup(raw_content, 'html.parser')
                for review_soup in soup.select('div[class=single-review]'):
                    review_content = review_soup.select_one('.review-body').text.strip()
                    review_date = review_soup.select_one('.review-date').text.strip()

                    if review_content is not None and len(review_content) > 5:
                        l_review_timestamp.append(get_timestamp(review_date, lang_code))
                        l_pre_review_content.append(review_content)

        # Get most recent 125 comments.
        np_review_timestamp = np.array(l_review_timestamp)
        for i in np.argsort(np_review_timestamp)[::-1][:125]:
            l_review_content.append(l_pre_review_content[i])

        l_req = []
        for content in l_review_content:
            l_req.append(grequests.post(language_detection_endpoint, data={'text': content}))

        l_result = grequests.map(l_req)
        d_count_lang_code = defaultdict(lambda: 0)
        for result in l_result:
            if result is None:
                continue

            try:
                d_result = json.loads(result.text.strip())
                if d_result['success']:
                    if len(d_result['languages']) == 0:
                        continue

                    detected_lang_code = d_result['languages'][0]['code']
                    if detected_lang_code not in HL_LANGUAGE_CODES:
                        continue

                    d_count_lang_code[detected_lang_code] += 1

            except Exception:
                pass

        three_largest = nlargest(3, d_count_lang_code, key=d_count_lang_code.get)
        return three_largest

    def __get_android_app_lang(self, bundle_id):
        l_url = []
        for lang_code in HL_LANGUAGE_CODES:
            l_url.append('https://play.google.com/store/apps/details?id={}&hl={}'.format(bundle_id, lang_code))

        rs = (grequests.get(u) for u in l_url)
        l_result = grequests.map(rs)
        s_description = set()
        for result in l_result:
            if result is None:
                continue
            raw_content = result.text.strip()

            soup = BeautifulSoup(raw_content, 'html.parser')
            selected = soup.find('meta', property='og:description')
            if selected is None:
                return None

            description = selected['content']
            s_description.add(description)

        l_req = []
        for description in s_description:
            l_req.append(grequests.post(language_detection_endpoint, data={'text': description}))

        l_result = grequests.map(l_req)

        s_detected_lang_code = set()
        for result in l_result:
            if result is None:
                continue

            try:
                d_result = json.loads(result.text.strip())
                if d_result['success']:
                    detected_lang_code = d_result['languages'][0]['code']
                    if detected_lang_code not in HL_LANGUAGE_CODES:
                        continue

                    s_detected_lang_code.add(detected_lang_code)
            except Exception:
                pass

        l_description_lang_code = list(s_detected_lang_code)
        return l_description_lang_code

    def __get_ios_app_lang(self, bundle_id):
        if not bundle_id.isdigit():
            res = requests.get('https://itunes.apple.com/lookup?bundleId={}'.format(bundle_id))
        else:
            res = requests.get('https://itunes.apple.com/lookup?id={}'.format(bundle_id))
        d_app_info = json.loads(res.text.strip())

        if d_app_info['resultCount'] < 1:
            return []

        d_app_info = d_app_info['results'][0]
        l_lang_code = [lang_code.lower() for lang_code in d_app_info['languageCodesISO2A']]
        return l_lang_code

