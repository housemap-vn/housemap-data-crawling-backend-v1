import tornado.ioloop
import tornado.options
import tornado.web
import argparse
import json

from internal.handler.get_ad import GetAdHandler
from internal.handler.get_ads import GetAdsHandler
from internal.handler.upsert_ad import UpsertAdHandler
from internal.handler.get_api_info import GetApiInfoHandler
from internal.handler.update_ad_live import UpdateAdLive

# greenlet error: https://github.com/miguelgrinberg/Flask-SocketIO/issues/65
from gevent import monkey
monkey.patch_all()


def make_app():
    return tornado.web.Application([
        (r"/get_ad", GetAdHandler),
        (r"/get_ads", GetAdsHandler),
        (r"/upsert_ad", UpsertAdHandler),
        (r"/get_api_info", GetApiInfoHandler),
        (r"/update_ad_live", UpdateAdLive)
    ])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Bid phrase generation app argument')
    parser.add_argument('--port', type=int, default=7000, help='API port')
    parser.add_argument('--n_workers', type=int, default=2, help='Number of subprocess')

    args = parser.parse_args()

    server = tornado.httpserver.HTTPServer(make_app())
    server.bind(args.port)
    server.start(args.n_workers)
    print("All Apis loaded")

    tornado.ioloop.IOLoop.current().start()
